/*
 * This file is part of sigxx
 * Copyright (C) 2022-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIGXX_OBJECT_HH
#define SIGXX_OBJECT_HH

#include <unordered_map>
#include "slot.hh"

namespace sigxx {

class object {

public:
    virtual ~object() {
        for ( auto&& destructor: destructors )
            destructor();
    }

private:
    template< typename... Args >
    friend class ::sigxx::signal;
    template< typename... Args, typename Object >
    slot<Args...>& sigxx_slot ( void (Object::*method) (Args...) ) {
        auto& s = method_slots[reinterpret_cast<void (*&) ()>(method)];
        if ( !s ) {
            auto* self = dynamic_cast<Object*> ( this );
            auto* a = new slot<Args...> ( method, self );
            s = a;
            destructors.push_front ( [a] { delete a; } );
        }
        return *static_cast<slot<Args...>*> ( s );
    }

    std::forward_list<std::function<void()>> destructors{};
    std::unordered_map<void (*) (), void*> method_slots{};
};

}

#endif // SIGXX_OBJECT_HH
