/*
 * This file is part of sigxx
 * Copyright (C) 2014-2023 Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIGXX_SLOT_HH
#define SIGXX_SLOT_HH

#include <mutex>
#include <forward_list>
#include <functional>

namespace sigxx
{

template< typename... >
class signal;

template< typename... Args >
class slot
{
    friend class signal<Args...>;
    std::function<void(Args...)> m_func;
    std::forward_list<signal<Args...>*> m_signals{};
    void* m_userdata{nullptr};
    signal<Args...>* m_sender{nullptr};
    std::mutex m_mutex{};

    template< class Object >
    struct simple_bind {
        void (Object::*method) (Args...);
        Object* obj;
        simple_bind(void (Object::*method) (Args...), Object* obj) : method{method}, obj{obj} {}
        void operator() (Args&&... args) { (obj->*method) ( std::forward<Args> (args)... ); }
    };

public:
    slot() : slot{[](Args&&...){}} {}

    slot (const slot&) = delete;

    template< typename Functor >
    slot ( Functor&& func ) : m_func{std::forward<Functor>(func)} {}

    template< typename Object >
    slot ( void (Object::*method) (Args...), Object* obj ) : m_func{simple_bind<Object>{method,obj}} {}

    ~slot();

    slot& operator= ( const slot& ) = delete;

    template< typename Functor >
    slot& operator= ( Functor&& func ) { m_func = std::forward<Functor>(func); return *this; }

    template<typename... Args2>
    void operator() ( Args2&&... args ) { m_func ( std::forward<Args2> ( args )... ); }

    template< typename Data = void >
    Data* userdata() { return static_cast<Data*> ( m_userdata ); }
};
}

#endif // SIGXX_SLOT_HH
