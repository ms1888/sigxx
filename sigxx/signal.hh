/*
 * This file is part of sigxx
 * Copyright (C) 2014-2023 Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIGXX_SIGNAL_HH
#define SIGXX_SIGNAL_HH

#include "slot.hh"

namespace sigxx
{

template< typename... Args >
class signal : public slot<Args...>
{
    friend class slot<Args...>;
    std::forward_list<slot<Args...>*> m_slots{};

public:
    signal ( void* userdata );
    ~signal();
    template< typename Object, typename Slot >
    void connect ( Object* receiver, Slot&& slot ) { connect ( receiver->template sigxx_slot<Args...> ( std::forward<Slot> ( slot ) ) ); }
    template< typename Object, typename Slot >
    void disconnect ( Object* receiver, Slot&& slot ) { disconnect ( receiver->template sigxx_slot<Args...> ( std::forward<Slot> ( slot ) ) ); }
    void connect ( slot<Args...>& slot ) { m_slots.push_front ( &slot ); slot.m_signals.push_front ( this ); }
    void disconnect ( slot<Args...>& slot ) { m_slots.remove ( &slot ); slot.m_signals.remove ( this ); }
};

template< typename... Args >
signal<Args...>::signal ( void* userdata )
    : slot<Args...> {
        [userdata,this] ( Args&&... args ) {
            for ( slot<Args...>* slot: m_slots ) {
                slot->m_mutex.lock();
                auto tmp1 = slot->m_userdata;
                auto tmp2 = slot->m_sender;
                slot->m_userdata = userdata;
                slot->m_sender = this;
                (*slot) ( std::forward<Args> ( args )... );
                slot->m_userdata = tmp1;
                slot->m_sender = tmp2;
                slot->m_mutex.unlock();
            }
        }
    } {}

template< typename... Args >
slot<Args...>::~slot()
{
    for ( signal<Args...>* signal: m_signals )
        signal->m_slots.remove ( this );
}

template< typename... Args >
signal<Args...>::~signal()
{
    for ( slot<Args...>* slot: m_slots )
        slot->m_signals.remove ( this );
}
}

#endif // SIGXX_SIGNAL_HH
