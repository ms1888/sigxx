/*
 * This file is part of sigxx
 * Copyright (C) 2014-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIGXX_REMOVE_EVERYTHING_HH
#define SIGXX_REMOVE_EVERYTHING_HH

namespace sigxx {
template<typename Type>
struct remove_everything {
private:
    template<typename,int> struct remove_everything_1;
    template<typename,int> struct remove_everything_2;
    template<typename T> struct remove_everything_1<T,2> { using type = T; };
    template<typename T> struct remove_everything_2<T,2> { using type = T; };
    template<typename T, int internal> struct remove_everything_1 : remove_everything_2<T,internal+1> {};
    template<typename T, int internal> struct remove_everything_1<T*,internal> : remove_everything_2<T,0> {};
    template<typename T, int internal> struct remove_everything_1<T&,internal> : remove_everything_2<T,0> {};
    template<typename T, int internal> struct remove_everything_1<T&&,internal> : remove_everything_2<T,0> {};
    template<typename T, int internal> struct remove_everything_1<T[],internal> : remove_everything_2<T,0> {};
    template<typename T, int internal> struct remove_everything_2 : remove_everything_1<T,internal+1> {};
    template<typename T, int internal> struct remove_everything_2<T const,internal> : remove_everything_1<T,0> {};
    template<typename T, int internal> struct remove_everything_2<T volatile,internal> : remove_everything_1<T,0> {};
    template<typename T, int internal> struct remove_everything_2<T const volatile,internal> : remove_everything_1<T,0> {};
    template<typename T, int internal, std::size_t size> struct remove_everything_1<T[size],internal> : remove_everything_1<T,0> {};

public:
    using type = typename remove_everything_1<Type,0>::type;
};
template<typename Type>
using remove_everything_t = typename remove_everything<Type>::type;
}

#endif // SIGXX_REMOVE_EVERYTHING_HH
