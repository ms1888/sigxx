/*
 * This file is part of sigxx
 * Copyright (C) 2022-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SIGXX_HH
#define SIGXX_HH

#include "sigxx/object.hh"
#include "sigxx/remove_everything.hh"
#include "sigxx/signal.hh"

#define SIGXX_CONNECT3(signal,receiver,slot) \
    (signal).connect ( receiver, &::sigxx::remove_everything_t<decltype(receiver)>::slot )
#define SIGXX_DISCONNECT3(signal,receiver,slot) \
    (signal).disconnect ( receiver, &::sigxx::remove_everything_t<decltype(receiver)>::slot )
#define SIGXX_CONNECT4(sender,signal,receiver,slot) \
    SIGXX_CONNECT3((sender)->signal, receiver, slot )
#define SIGXX_DISCONNECT4(sender,signal,receiver,slot) \
    SIGXX_DISCONNECT3((sender)->signal, receiver, slot )

#endif // SIGXX_HH
